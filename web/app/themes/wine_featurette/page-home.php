<?php 
use Roots\Sage\Titles; 

$blogFeedClass = 'blog-feed--home';
include(locate_template('templates/content-blog_feed.php'));
?>

<div class="services__home">
    <?php
    $titleOverride = 'Wine Offers & Services';
    include(locate_template('templates/page-header.php'));  
    ?>

    <section class="services">
        <div class="services__list">
            <?php 
            $query = new WP_Query([
                'post_type' => 'service', 
                'posts_per_page' => -1,
                'orderby' => 'menu_order',
                'order' => 'ASC'
            ]);

            while ($query->have_posts()) : $query->the_post();
                ?>
                <div class="services__item">
                    <h2 class="services__heading"><a href="<?= get_permalink(); ?>"><?= Titles\title(); ?></a></h2>

                    <div class="services__divider"></div>

                    <a href="<?= get_permalink(); ?>"><img src="<?= get_field('image')['sizes']['max']; ?>" /></a>
                </div>
                <?php
            endwhile; wp_reset_query(); 
            ?>
        </div>
    </section>
</div>
