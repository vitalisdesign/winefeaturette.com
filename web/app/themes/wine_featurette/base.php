<?php
use Roots\Sage\Setup;
use Roots\Sage\Assets;
use Roots\Sage\Wrapper;
?>

<!doctype html>
<html <?php language_attributes(); ?>>
    <?php 
    get_template_part('templates/head'); 

    if(! isset($_COOKIE['subscribed_to_blog'])) {
        $cookieClass = 'not-subscribed-to-blog';
    } else {
        $cookieClass = '';
    }
    ?>
    <body <?php body_class($cookieClass); ?>>
        <div class="container">
            <!--[if IE]>
                <div class="alert alert-warning">
                    <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
                </div>
            <![endif]-->
            
            <div class="page-container">
                <?php
                    do_action('get_header');
                    get_template_part('templates/header');
                ?>

                <main class="main">
                    <?php include Wrapper\template_path(); ?>
                </main>
            </div>

            <?php
                do_action('get_footer');
                get_template_part('templates/footer');
                wp_footer();
            ?>
        </div>

        <div class="mobile-overlay">
            <button class="mobile-overlay__close ui-button">
                <img src="<?= Assets\asset_path('images/mobile_overlay_close.svg'); ?>" />
            </button>

            <?php get_template_part('templates/header', 'nav'); ?>
        </div>
    </body>
</html>
