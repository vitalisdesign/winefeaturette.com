function trackFormSubmission() {
    if (typeof ga !== 'undefined') {
        ga('send', 'event', 'CTA', 'Click', 'Blog Subscription');
    }
}

jQuery(document).ready(function() {

    jQuery('.blog-cta').submit(function() {
        var form = jQuery(this);
        var data = form.serializeArray();
        data.push({ 
            'name': 'action',
            'value': 'processBlogSubscription'
        });

        jQuery.ajax({
            type: 'POST',
            url: ajax.url,
            data: jQuery.param(data),
            dataType: 'json',
            beforeSend: function() {
                jQuery(form).find('button[type=submit]').attr('disabled', true).addClass('loading');
            },
            success: function(response) {
                trackFormSubmission();
                jQuery('.blog-cta__copy').text(response.message);
                jQuery(form).find('button[type=submit]').addClass('disabled');
                jQuery('body').removeClass('not-subscribed-to-blog');

                if (jQuery(form).hasClass('blog-cta--single')) {
                    jQuery(form).fadeOut(200);
                    jQuery(form).prev().fadeOut(200);
                }
            },
            error: function(response) {
                jQuery(form).find('.error').removeClass('error');

                jQuery.each(response.responseJSON.errors.error_data, function(key, value) {
                    jQuery(form).find('#' + value.form_field).addClass('error');
                });

                jQuery(form).find('.error').first().focus();
                jQuery(form).find('button[type=submit]').removeAttr('disabled');
            },
            complete: function() {
                jQuery(form).find('button[type=submit]').removeClass('loading');
            }
        });

        return false;
    });
});
