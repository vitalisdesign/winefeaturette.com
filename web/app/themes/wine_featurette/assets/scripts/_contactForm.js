function trackFormSubmission() {
    if (typeof ga !== 'undefined') {
        ga('send', 'event', 'CTA', 'Click', 'Contact Form Submission');
    }
}

jQuery(document).ready(function() {

    jQuery('.contact-form').submit(function() {
        var form = jQuery(this);
        var data = form.serializeArray();
        data.push({ 
            'name': 'action',
            'value': 'processContactForm'
        });

        jQuery.ajax({
            type: 'POST',
            url: ajax.url,
            data: jQuery.param(data),
            dataType: 'json',
            beforeSend: function() {
                jQuery(form).find('button[type=submit]').attr('disabled', true).addClass('loading');
            },
            success: function(response) {
                trackFormSubmission();
                jQuery(form).find('.contact-form__feedback').html(response.message).hide().fadeIn(200);
            },
            error: function(response) {
                jQuery(form).find('.error').removeClass('error');

                jQuery.each(response.responseJSON.errors.error_data, function(key, value) {
                    jQuery(form).find('#' + value.form_field).addClass('error');
                });

                jQuery(form).find('.error').first().focus();
                jQuery(form).find('button[type=submit]').removeAttr('disabled');
            },
            complete: function() {
                jQuery(form).find('button[type=submit]').removeClass('loading');
            }
        });

        return false;
    });
});
