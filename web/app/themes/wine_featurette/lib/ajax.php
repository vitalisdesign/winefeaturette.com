<?php
namespace Roots\Sage\Ajax;

use WP_Error;
use \DrewM\MailChimp\MailChimp;

/**
 * Process a blog subscription.
 */
function processBlogSubscription() {
    $firstName = sanitize_text_field($_POST['first_name']);
    $email = $_POST['email'];

    $errors = new WP_Error();

    if (empty($firstName)) {
        $errors->add('first_name_empty', 'Please enter your first name', ['form_field' => 'first-name']);
    }

    if (empty($email) || !is_email($email)) {
        $errors->add('email_invalid', 'Please enter a valid email address', ['form_field' => 'email']);
    }

    if (! $errors->get_error_code()) {
        $email = sanitize_email($email);

        $MailChimp = new MailChimp(env('MAILCHIMP_KEY'));
        $listId = '075b9c74ad';
        $subscriberHash = $MailChimp->subscriberHash($email);

        $result = $MailChimp->put("lists/$listId/members/$subscriberHash", [
            'email_address' => $email,
            'merge_fields' => [
                "FNAME" => $firstName
            ],
            'status' => 'subscribed'
        ]);

        setcookie(
            'subscribed_to_blog',
            $firstName,
            time() + (10 * 365 * 24 * 60 * 60),
            COOKIEPATH
        );

        $response = [
            'message' => 'Thanks for subscribing to the Wine Featurette!',
            'result' => $result
        ];
    } else {
        $response['errors'] = $errors;
        status_header(422);
    }

    echo json_encode($response);
    exit();
}
add_action('wp_ajax_processBlogSubscription', __NAMESPACE__ . '\\processBlogSubscription');
add_action('wp_ajax_nopriv_processBlogSubscription', __NAMESPACE__ . '\\processBlogSubscription');

/**
 * Process a contact form submission.
 */
function processContactForm() {
    $name = sanitize_text_field($_POST['name']);
    $email = $_POST['email'];
    $comments = sanitize_text_field($_POST['comments']);

    $errors = new WP_Error();

    if (empty($name)) {
        $errors->add('name_empty', 'Please enter your name', ['form_field' => 'name']);
    }

    if (empty($email) || !is_email($email)) {
        $errors->add('email_invalid', 'Please enter a valid email address', ['form_field' => 'email']);
    }

    if (empty($comments)) {
        $errors->add('comments_empty', 'Please write a short message', ['form_field' => 'comments']);
    }

    if (! $errors->get_error_code()) {
        $email = sanitize_email($email);

        $to = get_option('admin_email');
        $subject = 'You\'ve got a new contact form submission!';
        $headers = [
            'From: Wine Featurette <wordpress@winefeaturette.com>',
            'Content-Type: text/html; charset=UTF-8'
        ];

        ob_start(); ?>

        We just got a new contact form submission from winefeaturette.com!<br /><br />

        Name: <strong><?= $name; ?></strong><br />
        Email address: <a href="mailto:<?= $email; ?>"><?= $email; ?></a><br /><br />

        Comments:<br />
        <em><?= $comments; ?></em><br /><br />

        <?php $message = ob_get_clean();

        $result = wp_mail($to, $subject, $message, $headers);

        $response = [
            'message' => 'Thanks for your message! I\'ll be in touch soon.',
            'result' => $result
        ];
    } else {
        $response['errors'] = $errors;
        status_header(422);
    }

    echo json_encode($response);
    exit();
}
add_action('wp_ajax_processContactForm', __NAMESPACE__ . '\\processContactForm');
add_action('wp_ajax_nopriv_processContactForm', __NAMESPACE__ . '\\processContactForm');
