<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup() {
    // Enable features from Soil when plugin is activated
    // https://roots.io/plugins/soil/
    add_theme_support('soil-clean-up');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-relative-urls');

    // Make theme available for translation
    // Community translations can be found at https://github.com/roots/sage-translations
    load_theme_textdomain('sage', get_template_directory() . '/lang');

    // Enable plugins to manage the document title
    // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
    add_theme_support('title-tag');

    // Register wp_nav_menu() menus
    // http://codex.wordpress.org/Function_Reference/register_nav_menus
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage')
    ]);

    // Enable post thumbnails
    // http://codex.wordpress.org/Post_Thumbnails
    // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
    // http://codex.wordpress.org/Function_Reference/add_image_size
    add_theme_support('post-thumbnails');
    add_image_size('max', 1600, 900);

    // Enable HTML5 markup support
    // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    // Use main stylesheet for visual editor
    // To add custom styles edit /assets/styles/layouts/_tinymce.scss
    add_editor_style(Assets\asset_path('styles/main.css'));

    show_admin_bar(false);
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Custom image sizes.
 */
function my_image_sizes($sizes) {
    $add_sizes = [
        'max' => __('Max')
    ];
    $newsizes = array_merge($sizes, $add_sizes);
    
    return $newsizes;
}
add_filter('image_size_names_choose', __NAMESPACE__ . '\\my_image_sizes');

/**
 * Register custom post types
 */
function custom_post_types() {
    register_post_type('service', [
        'labels' => [
            'name' => __('Services'),
            'add_new_item' => __('Add New Service'),
            'search_items' => __('Search Services'),
            'not_found' => __('No services found.'),
            'singular_name' => __('Service')
        ],
        'public' => true,
        'hierarchical' => true,
        'menu_icon' => 'dashicons-portfolio',
        'menu_position' => 20,
        'rewrite' => ['slug' => 'services', 'with_front' => false],
        'supports' => ['title']
    ]);

    register_post_type('event', [
        'labels' => [
            'name' => __('Events'),
            'add_new_item' => __('Add New Event'),
            'search_items' => __('Search Event'),
            'not_found' => __('No events found.'),
            'singular_name' => __('Event')
        ],
        'public' => true,
        'hierarchical' => true,
        'menu_icon' => 'dashicons-calendar-alt',
        'menu_position' => 21,
        'rewrite' => ['slug' => 'events'],
        'supports' => ['title']
    ]);
}
add_action('init', __NAMESPACE__ . '\\custom_post_types');

/**
 * Add ACF options page.
 */
if (function_exists('acf_add_options_page')) {
    acf_add_options_page([
        'page_title' => 'Options',
        'position' => 23,
    ]);
}

/**
 * Custom nav menu classes.
 */
function add_custom_class($classes = [], $item = false) {
    $blog_page = get_post(get_page_by_path('blog'));
    if ($item->title == $blog_page->post_title && ! is_page() && ! in_array('current-menu-item', $classes)) {
        $classes[] = 'current-menu-item';        
    }               
    return $classes;
}
add_filter('nav_menu_css_class', __NAMESPACE__ . '\\add_custom_class', 100, 2);

/**
 * Register sidebars
 */
function widgets_init() {
    register_sidebar([
        'name'          => __('Primary', 'sage'),
        'id'            => 'sidebar-primary',
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ]);

    register_sidebar([
        'name'          => __('Footer', 'sage'),
        'id'            => 'sidebar-footer',
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ]);
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
    static $display;

    isset($display) || $display = !in_array(true, [
        // The sidebar will NOT be displayed if ANY of the following return true.
        // @link https://codex.wordpress.org/Conditional_Tags
        is_404(),
        is_front_page(),
        is_page_template('template-custom.php'),
    ]);

    return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets() {
    wp_enqueue_style('sage/css', Assets\asset_path('styles/main.css'), false, null);

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    wp_enqueue_script('sage/js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);
    wp_localize_script('sage/js', 'ajax', ['url' => admin_url('admin-ajax.php')]);
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);

/**
 * Insert code into header via wp_head.
 */
add_action('wp_head', function() {
  if (env('WP_ENV') == 'production' && ! is_user_logged_in()) {
    ?>
    
    <?php
  }
});
