<?php
if(! isset($_COOKIE[COOKIE_SUBSCRIBED_TO_BLOG])) {
    $blogCtaClass = 'blog-cta--feed';
    include(locate_template('templates/content-blog_cta.php'));
}
?>

<div class="blog-feed <?= $blogFeedClass; ?>">
    <?php
    if ($blogFeedClass == 'blog-feed--home') {
        $postPerPage = 3;
    } else {
        $postPerPage = 5;
    }

    $tag = get_queried_object();

    $query = new WP_Query([
        'post_type' => 'post',
        'tag' => $tag->slug,
        'posts_per_page' => $postPerPage,
        'paged' => get_query_var('paged') ? get_query_var('paged') : 1
    ]);

    while ($query->have_posts()) : $query->the_post();
        get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format());
    endwhile; wp_reset_query();
    ?>
</div>

<div class="blog-feed__links">
    <?php
    if ($blogFeedClass == 'blog-feed--home') {
        ?>
        <a href="<?php the_permalink(get_page_by_path('blog')); ?>">All Posts &rsaquo;</a>
        <?php
    } else {
        echo paginate_links([
            'current' => max( 1, get_query_var('paged') ),
            'total' => $query->max_num_pages,
            'prev_text' => __('&lsaquo; Newer'),
            'next_text' => __('Older &rsaquo;')
        ]);
    }
    ?>
</div>
