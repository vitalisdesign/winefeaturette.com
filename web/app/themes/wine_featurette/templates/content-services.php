<?php
use Roots\Sage\Titles; 
?>

<section class="services">
    <div class="services__list">
        <?php 
        $query = new WP_Query([
            'post_type' => 'service', 
            'posts_per_page' => -1,
            'orderby' => 'menu_order',
            'order' => 'ASC'
        ]);

        while ($query->have_posts()) : $query->the_post();
            ?>
            <div class="services__item">
                <h2 class="services__heading"><a href="<?= the_permalink(); ?>"><?= Titles\title(); ?></a></h2>

                <div class="services__description"><?php the_field('description'); ?></div>

                <div class="services__divider"></div>

                <a href="<?= the_permalink(); ?>"><img src="<?= get_field('image')['sizes']['max']; ?>" /></a>
            </div>
            <?php
        endwhile; wp_reset_query(); 
        ?>
    </div>

    <div class="services__cta">
        <a class="ui-button ui-button--primary" href="<?= get_permalink(get_page_by_path('contact')); ?>">Searching for Wine?</a>
    </div>
</section>
