<article <?php post_class('blog-feed__item'); ?>>
    <?php
    if (has_post_thumbnail()) {
        $featuredImage = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large');
        ?>
        <a class="blog-feed__item__image" href="<?php the_permalink(); ?>" style="background-image: url(<?= $featuredImage[0]; ?>);">
        </a>
        <?php
    }
    ?>
    <div class="blog-feed__item__content">
        <header>
            <h2 class="blog-feed__item__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <?php get_template_part('templates/entry-meta'); ?>
        </header>

        <div class="blog-feed__item__excerpt">
            <?php the_excerpt(); ?>
        </div>
    </div>
</article>
