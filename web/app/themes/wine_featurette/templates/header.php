<header class="header">
    <a class="header__logo" href="<?= esc_url(home_url('/')); ?>">
        <div class="header__logo__heading">Rosemary S Gray</div>
        <div class="header__logo__subheading">Wine Featurette</div>
    </a>

    <a class="header__menu-button ui-button ui-button--secondary">Menu</a>

    <?php get_template_part('templates/header', 'nav'); ?>

    <?php get_template_part('templates/content', 'aside'); ?>
</header>
