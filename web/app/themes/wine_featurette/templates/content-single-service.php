<?php
use Roots\Sage\Titles; 
?>

<?php get_template_part('templates/page', 'header'); ?>

<div class="services-single">
    <div class="services-single__description"><?php the_field('description'); ?></div>

    <div class="services__divider"></div>

    <img src="<?= get_field('image')['sizes']['max']; ?>" />
</div>
