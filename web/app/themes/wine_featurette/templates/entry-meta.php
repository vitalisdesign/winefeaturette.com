<div class="post__meta">
    <time class="post__date-posted" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>

    <?php the_tags('&nbsp; | &nbsp;'); ?>
</div>
