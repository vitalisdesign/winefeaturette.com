<footer class="footer">
    <?php get_template_part('templates/content', 'aside'); ?>

    <div class="footer__legal">
        <p>&copy; <?php echo date('Y') . ' Rosemary S Gray. ' . __('All Rights Reserved'); ?>.</p>

        <p>
            <span class="footer__links">
                <?php $shipping_policy = get_page_by_path('shipping-policy'); ?>
                <a href="<?= get_permalink($shipping_policy); ?>"><?= $shipping_policy->post_title; ?></a>

                <span class="divider">|</span>

                <?php $terms_and_conditions = get_page_by_path('terms-and-conditions'); ?>
                <a href="<?= get_permalink($terms_and_conditions); ?>"><?= $terms_and_conditions->post_title; ?></a>

                <span class="divider">|</span>

                <?php $privacy_policy = get_page_by_path('privacy-policy'); ?>
                <a href="<?= get_permalink($privacy_policy); ?>"><?= $privacy_policy->post_title; ?></a>
            </span>
            
        </p>
        
        <p class="footer__credit">
            Website Created by <a href="https://www.vitalisdesign.com" target="_blank" title="Website Created by Vitalis Design">Vitalis Design</a>.
        </p>
    </div>
</footer>
