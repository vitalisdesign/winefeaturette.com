<?php while (have_posts()) : the_post(); ?>
    <article <?php post_class('blog-single'); ?>>
        <?php
        if (has_post_thumbnail()) {
            ?>
            <div class="blog-single__image">
                <?php the_post_thumbnail('max'); ?>
            </div>
            <?php
        }
        ?>

        <header>
            <h1 class="blog-single__title"><?php the_title(); ?></h1>
            <?php get_template_part('templates/entry-meta'); ?>
        </header>

        <div class="blog-single__content">
            <?php the_content(); ?>
        </div>
    </article>
<?php endwhile; ?>

<?php
if(! isset($_COOKIE[COOKIE_SUBSCRIBED_TO_BLOG])) {
    ?>
    <div class="blog-cta__header">
        <div class="blog-cta__header__squiggly blog-cta__header__squiggly-left"></div>
        <h3 class="blog-cta__header__heading">Subscribe below to read full article</h3>
        <div class="blog-cta__header__squiggly blog-cta__header__squiggly-right"></div>
    </div>

    <?php
    $blogCtaClass = 'blog-cta--single';
    include(locate_template('templates/content-blog_cta.php'));
}
?>
