<?php 
use Roots\Sage\Titles; 

if (isset($titleOverride)) {
    $title = $titleOverride;
} else {
    $title = Titles\title();
}
?>

<div class="page-header">
    <div class="page-header__squiggly page-header__squiggly-left"></div>
    <h1 class="page-header__headline"><?= $title; ?></h1>
    <div class="page-header__squiggly page-header__squiggly-right"></div>
</div>
