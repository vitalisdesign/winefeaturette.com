<?php use Roots\Sage\Assets; ?>

<div class="aside">
    <div class="aside__follow-me">
        <h3 class="aside__heading">Follow Wine Featurette</h3>

        <div class="aside__follow-me__icons">
            <a href="https://www.facebook.com/winefeaurette" target="_blank">
                <img src="<?= Assets\asset_path('images/icons/facebook.svg'); ?>" />
            </a>

            <a href="https://www.instagram.com/winefeaturette" target="_blank">
                <img src="<?= Assets\asset_path('images/icons/instagram.svg'); ?>" />
            </a>

            <a href="https://twitter.com/winefeaturette" target="_blank">
                <img src="<?= Assets\asset_path('images/icons/twitter.svg'); ?>" />
            </a>
        </div>
    </div>

    <div class="aside__divider"></div>

    <div class="aside__events">
        <h3 class="aside__heading">Upcoming Events</h3>

        <?php 
        $query = new WP_Query([
            'post_type' => 'event', 
            'posts_per_page' => 3,
            'orderby' => 'meta_value_num',
            'meta_key' => 'date',
            'order' => 'ASC'
        ]);

        while ($query->have_posts()) : $query->the_post();
            ?>
            <a class="events__item" href="<?php the_field('link'); ?>" target="_blank">
                <span class="events__date">
                    <?php
                    $dateArray = explode(' ', trim(get_field('date')));
                    ?>
                    <span class="month"><?= $dateArray[0]; ?></span>
                    <span class="day"><?= $dateArray[1]; ?></span>
                </span>

                <h3 class="events__name"><?php the_field('name'); ?></h3>
            </a>
            <?php
        endwhile; wp_reset_query(); 
        ?>
    </div>
</div>
