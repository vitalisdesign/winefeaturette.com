<div class="contact">
    <div class="contact__intro">
        <div class="contact__copy">
            <?php the_content(); ?>
        </div>
    </div>

    <form class="contact-form" novalidate>
        <div class="contact-form__text-fields">
            <fieldset>
                <?php $namePlaceholder = 'Your name'; ?>
                <label for="name" class="sr-only"><?= $namePlaceholder; ?></label>
                <input type="text" name="name" id="name" placeholder="<?= $namePlaceholder; ?>">
            </fieldset>

            <fieldset>
                <?php $emailPlaceholder = 'Your email address'; ?>
                <label for="email" class="sr-only"><?= $emailPlaceholder; ?></label>
                <input type="email" name="email" id="email" placeholder="<?= $emailPlaceholder; ?>">
            </fieldset>
        </div>

        <fieldset class="contact-form__textarea">
            <?php $commentsPlaceholder = 'Your message...'; ?>
            <label for="comments" class="sr-only"><?= $commentsPlaceholder; ?></label>
            <textarea name="comments" id="comments" placeholder="<?= $commentsPlaceholder; ?>" rows="4"></textarea>
        </fieldset>

        <div class="contact-form__cta">
            <button type="submit" class="contact-form__submit ui-button ui-button--primary">Send Message</button>

            <div class="contact-form__feedback"></div>
        </div>
    </form>
</div>
