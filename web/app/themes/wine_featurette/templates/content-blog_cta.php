<form class="blog-cta <?= $blogCtaClass; ?>" novalidate>
    <div class="blog-cta__copy"><?= get_field('wine_featurette_subscription_copy', 'option'); ?></div>
    
    <div class="blog-cta__fields">
        <fieldset>
            <?php $firstNamePlaceholder = 'Your first name'; ?>
            <label for="first-name" class="sr-only"><?= $firstNamePlaceholder; ?></label>
            <input type="text" name="first_name" id="first-name" placeholder="<?= $firstNamePlaceholder; ?>">
        </fieldset>

        <fieldset>
            <?php $emailPlaceholder = 'Your email address'; ?>
            <label for="email" class="sr-only"><?= $emailPlaceholder; ?></label>
            <input type="email" name="email" id="email" placeholder="<?= $emailPlaceholder; ?>">
        </fieldset>

        <button type="submit" class="blog-cta__submit ui-button ui-button--primary">Subscribe</button>
    </div>
</form>
