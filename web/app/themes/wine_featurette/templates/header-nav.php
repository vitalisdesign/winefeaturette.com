<nav class="header__nav">
    <?php
    if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'container' => '']);
    endif;
    ?>
</nav>
